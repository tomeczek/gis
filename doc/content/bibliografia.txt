\bibitem{flaskbook} Daniel Gaspar Jack Stouffer, 2018, Mastering Flask Web Development Second Edition,ISBN 978-1-78899-540-5
%   First published: September 2015 Second Edition: October 2018
\bibitem{flask} Dokumentacja Flaska
\url{http://flask.pocoo.org/docs/1.0/}
\bibitem{docker} Oficialna strona Dockera
\url{https://docs.docker.com/get-started/}
\bibitem{dockercompose} Dokumentacja Docker Compose
\url{https://docs.docker.com/compose/}
\bibitem{jinja} Dokumentacja silnika szablonów Jinja2
\url{http://jinja.pocoo.org/docs/2.10/}
\bibitem{SQLAlchemy} Dokumentacja SQLAlchemy
\url{https://docs.sqlalchemy.org/en/rel_1_2/} 
\bibitem{psycopg} Dokumentacja PostgreSQL
\url{https://wiki.postgresql.org/wiki/Main_Page}
\bibitem{psqlLicence} Licencja PostgreSQL
\url{https://www.postgresql.org/about/licence/}
