%\subsection{Sprawozdanie 1}
Celem projektu jest realizacja graficznego edytora grafów. Do wymagań funkcjonalnych projektu zalicza się:
\begin{itemize}
\item Graf powinien być tworzony tak, aby spełniał warunki ekonomicznego wydruku. Powinien zatem być zobrazowany przy użyciu dwóch kolorów (czarny i biały). Czarny kolor powinien stanowić jak najmniejszą część wydruku, a zatem należy zadbać o odpowiednią grubość linii i czcionek. 
\item Groty w grafie powinny mieć odpowiedni rozmiar. Należy zadbać o czytelność.
\item Krawędzie grafu powinny być liniami prostymi. Wyjątek stanowi przypadek, w którym pomiędzy dwoma węzłami istnieją krawędzie wielokrotne. W takim przypadku krawędź reprezentowana jest poprzez łuk.
\item Dopuszcza się, że pomiędzy dwoma węzłami istnieją krawędzie jednokrotne lub dwukrotne.
\item Napisy na wierzchołkach powinny mieć taką wielkość, aby po wydrukowaniu grafu można było bez trudności odczytać ich treść.
\item Etykiety wierzchołków i krawędzi powinny stanowić dowolny string złożony maksymalnie z dwóch znaków.
\item Etykiety wierzchołków powinny znajdować się wewnątrz nich, zaś etykiety krawędzi powinny być umiejscowione nad krawędzią.
\item Użytkownik powinien mieć możliwość ustawiania parametrów poprzez menu (nie korzystać z pliku konfiguracyjnego).
\item Program powinien umożliwiać tworzenie maksymalnie dwóch krawędzi pomiędzy każdą parą węzłów.
\item Program powinien umożliwiać tworzenie co najwyżej jednej pętli własnej do każdego wierzchołka.
\item Program nie powinien umożliwiać obsługi grafów mieszanych.
\end{itemize}

Funkcje udostępniane dla użytkownika zostały zobrazowane poprzez diagram przypadków użycia.

\begin{figure}[h]
\includegraphics[width=16cm]{content/img/usecase.png}
\end{figure}


Wymagania niefunkcjonalne:
\begin{itemize}
\item Program powinien być dostarczony w postaci pliku wykonywalnego, możliwego do uruchomienia na komputerze działającym pod kontrolą systemu operacyjnego Windows 7. Program powinien być dostarczony na nośniku typu pen-drive i uruchomiony bez konieczności instalowania go na komputerze.
\item Program nie powinien być tworzony w języku Python - sugerowanymi technologiami są C++, Java.
\end{itemize}


%\subsection{Sprawozdanie 2}
%Postęp prac.
%\subsection{Sprawozdanie 3}
%Instrukcja z przykładami na zrzutach ekranu.


