﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeLine : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetThickness();
    }

    public void SetThickness()
    {
        graph.SetLineThickness( 1 - slider.value);
    }
}
