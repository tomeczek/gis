﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSize : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetArrowSize();
    }

    public void SetArrowSize()
    {
        graph.SetArrowThickness( slider.value);
    }
}
