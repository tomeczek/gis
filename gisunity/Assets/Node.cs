﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Node : MonoBehaviour
{
    public GameObject center;
    public GameObject node;

    public TextMeshPro label;
    private HashSet<IEdge> incidenceEdges = new HashSet<IEdge>();
    public bool HasLoop = false;

    internal void AddEdge(IEdge edge)
    {
        incidenceEdges.Add(edge);
    }

    internal void RemoveEdge(IEdge edge)
    {
        incidenceEdges.Remove(edge);
    }

    public HashSet<IEdge> GetIncidenceEdges()
    {
        return new HashSet<IEdge>(incidenceEdges);
    }

    internal void SetLineThickness(float thickness)
    {
        var scale = center.transform.localScale;
        scale.x = thickness;
        scale.y = thickness;
        center.transform.localScale = scale;
    }

    internal void SetNodeDiameter(float diameter)
    {
        var scale = node.transform.localScale;
        scale.x = diameter;
        scale.y = diameter;
        node.transform.localScale = scale;
    }

    internal void SetRotation(Quaternion rotation)
    {
        transform.rotation = rotation;
    }

    internal void SetTextSize(float value)
    {
        label.transform.localScale = new Vector3(value, value, value);
    }
}

