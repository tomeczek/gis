﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransform : MonoBehaviour
{
   public void UpdateRotation(Vector3 eulerAngles)
    {
        transform.eulerAngles += eulerAngles;
    }

    public void UpdatePosition(Vector3 translation)
    {
        transform.Translate(translation, Space.World);
    }
}
