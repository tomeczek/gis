﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Graph : MonoBehaviour
{
    private HashSet<Node> nodes = new HashSet<Node>();
    private HashSet<IEdge> edges = new HashSet<IEdge>();
    public GameObject nodePrefab;

    public GameObject straightEdgePrefab;
    public GameObject loopEdgePrefab;
    public GameObject bowEdgePrefab;
    public SpriteShape spriteShape;
    public Texture2D texture;

    public float nodeLineThickness;
    private Quaternion graphRotation;
    private float nodeDiameter;
    private bool isDirectedGraph;
    private float arrowThickness;
    private float arrowLenght;
    private float edgeLineThickness;
    private float textSize;

    internal void AddNode(Node node)
    {
        nodes.Add(node);
    }

    public void SetGraphRotation(Quaternion rotation)
    {
        graphRotation = rotation; 
        SetNodesRotation(nodes, rotation);
        SetEdgesRotation(edges, rotation);
        AdjustEdgesTransform(edges);
    }

    private void SetEdgesRotation(IEnumerable<IEdge> edgesToSet,Quaternion rotation)
    {
        foreach (var edge in edgesToSet)
        {
            edge.SetRotation(rotation);
        }
    }

    private void SetNodesRotation(IEnumerable<Node> nodesToSet, Quaternion rotation)
    {
        foreach ( var node in nodesToSet)
        {
            node.SetRotation(rotation);
        }
    }

    internal void SetDirectedGraph(bool isOn)
    {
        isDirectedGraph = isOn;
        SetDirectedEdges(isOn, edges);
    }

    private void SetDirectedEdges(bool isOn, IEnumerable<IEdge> edgesToSet)
    {
        foreach (var edge in edgesToSet)
        {
            edge.SetDirectedGraph(isOn);
            edge.AdjustTransform(arrowLenght, isDirectedGraph, GetNodeRadius());
        }
    }

    private void AdjustEdgesTransform(IEnumerable<IEdge> edgesToSet)
    {
        foreach (var edge in edgesToSet)
        {
            edge.AdjustTransform(arrowLenght,isDirectedGraph, GetNodeRadius());
        }
    }

    internal void SetNodeDiameter(float scale)
    {
        nodeDiameter = scale;
        foreach (var node in nodes)
        {
            node.SetNodeDiameter(scale);
        }
        AdjustEdgesTransform(edges);
    }

    public void SetLineThickness(float thickness)
    {
        nodeLineThickness = thickness; 
        foreach (var node in nodes)
        {
            node.SetLineThickness(thickness);
        }
    }
    public void SetArrowThickness(float thickness)
    {
        arrowThickness = thickness;
        foreach (var edge in edges)
        {
            edge.SetArrowThickness(thickness);
        }
    }

    internal void SetArrowLenght(float value)
    {
        arrowLenght = value;
        AdjustEdgesTransform(edges);
    }

    internal GameObject CreateNode(Vector3 position)
    {
        var node = Instantiate(nodePrefab, position, Camera.main.transform.rotation);
        var nodeScript = node.GetComponent<Node>();
        nodes.Add(nodeScript);
        nodeScript.SetLineThickness(nodeLineThickness);
        nodeScript.SetNodeDiameter(nodeDiameter);
        nodeScript.SetRotation(graphRotation);
        nodeScript.SetTextSize(textSize);
        return node;
    }

    internal void CreateEdge(GameObject lastClickedGraphElement, GameObject clickedGraphElement)
    {
        var nodes = new Tuple<Node, Node>(
            lastClickedGraphElement.GetComponent<Node>(),
            clickedGraphElement.GetComponent<Node>());
        if (nodes.Item1 == nodes.Item2 )
        {
            if (!nodes.Item1.HasLoop)
            {
                CreateLoopEdge(nodes);
                return;
            }
            else
            {
                return;
            }
        }
        var commonEdges = GetCommonEdges(nodes);
        if (commonEdges.Count == 0)
        {
            CreateStraightEdge(nodes);
            return;
        }
        if (commonEdges.Count == 1)
        {
            foreach (var e in commonEdges)
            {
                RemoveEdge(e);
            }
            CreateDoubleEdge(nodes);
            CreateDoubleEdge(new Tuple<Node, Node>(nodes.Item2, nodes.Item1));

            return;
        }
    }

    private HashSet<IEdge> GetCommonEdges(Tuple<Node, Node> nodes)
    {
        var commonEdges = nodes.Item1.GetIncidenceEdges();
        commonEdges.IntersectWith(nodes.Item2.GetIncidenceEdges());
        return commonEdges;
    }

    private void CreateDoubleEdge(Tuple<Node, Node> nodes)
    {
        var edge = Instantiate(bowEdgePrefab, Vector3.zero, Quaternion.identity);
        EdgeInicialization(edge, nodes);
        edge.GetComponent<BowEdge>().SetLabel(nodes);
    }

    private void CreateLoopEdge(Tuple<Node, Node> nodes)
    {
        var edge = Instantiate(loopEdgePrefab, Vector3.zero, Quaternion.identity);
        EdgeInicialization(edge, nodes);
    }

    private void CreateStraightEdge(Tuple<Node, Node> nodes)
    {
        var edge = Instantiate(straightEdgePrefab, Vector3.zero, Quaternion.identity);
        EdgeInicialization(edge, nodes);
    }

    private void EdgeInicialization(GameObject edge, Tuple<Node, Node> nodes)
    {
        var edgeScript = edge.GetComponent<IEdge>();
        nodes.Item1.AddEdge(edgeScript);
        nodes.Item2.AddEdge(edgeScript);
        edgeScript.SetNodes(nodes);
        edges.Add(edgeScript);
        edgeScript.SetArrowThickness(arrowThickness);
        edgeScript.SetDirectedGraph(isDirectedGraph);
        edgeScript.SetLineThickness(edgeLineThickness);
        edgeScript.AdjustTransform(arrowLenght, isDirectedGraph, GetNodeRadius());
        edgeScript.AdjustTransform(arrowLenght, isDirectedGraph, GetNodeRadius()); // werid flex but ok
        edgeScript.SetRotation(graphRotation);
        edgeScript.SetTextSize(textSize);
    }

    internal void TranslateGraphElement(GameObject dragGraphElement, Vector3 positionDelta)
    {
        var node = dragGraphElement.GetComponent<Node>();
        if (node)
        {
            TranslateNode(node, positionDelta);
            return;
        }
        var edge = dragGraphElement.GetComponent<IEdge>();
        if(edge !=  null)
        {
            TranslateLoopEdge(edge, positionDelta);
            return;
        }
    }

    private void TranslateNode(Node node, Vector3 positionDelta)
    {
        node.transform.Translate(positionDelta, Space.World);
        var nodeEdges = node.GetIncidenceEdges();
        SetEdgesRotation(nodeEdges,graphRotation);
        AdjustEdgesTransform(nodeEdges);
    }

    private void TranslateLoopEdge(IEdge loopEdge, Vector3 positionDelta)
    {
        loopEdge.Translate(positionDelta, isDirectedGraph, GetNodeRadius());
        loopEdge.SetRotation(graphRotation);
    }

    internal void DeleteGraphElement(GameObject e)
    {
        var node = e.GetComponent<Node>();
        if(node)
        {
            RemoveNode(node);
        }
        var edge = e.GetComponent<IEdge>();
        if (edge != null)
        {
           RemoveEdge(edge);
        }
    }

    private void RemoveNode(Node node)
    {
        var edges = node.GetIncidenceEdges();
        DeleteEdges(edges);
        nodes.Remove(node);
        node.gameObject.SetActive(false);
        Destroy(node.gameObject);
    }


    private void DeleteEdges(HashSet<IEdge> incidenceEdges)
    {
        foreach (var edge in incidenceEdges)
        {
            RemoveEdge(edge);
        }
    }

    private void RemoveEdge(IEdge edge)
    {
        edge.Unlink();
        edges.Remove(edge);

        ((MonoBehaviour)edge).gameObject.SetActive(false);
        Destroy(((MonoBehaviour)edge).gameObject);
    }

    internal void SetEdgeThickness(float thickness)
    {
        edgeLineThickness = thickness;
        foreach (var edge in edges)
        {
            edge.SetLineThickness(thickness);
        }
        Sprite sprite = Sprite.Create(texture,
            new Rect(0, 0, texture.width, texture.height),
            new Vector2(0.5f, 0.5f), 1f / (thickness / 32f));
        spriteShape.angleRanges[0].sprites[0] = sprite;
    }

    internal float GetNodeRadius()
    {
        return nodeDiameter / 2.0f;
    }


    internal void SetTextSize(float value)
    {
        textSize = value;
        foreach (var edge in edges)
        {
            edge.SetTextSize(value);
        }
        foreach (var node in nodes)
        {
            node.SetTextSize(value);
        }
    }

    internal void SetLoopSize(float size)
    {
        LoopEdge.loopSize = size;
        AdjustEdgesTransform(edges); //loop edges
    }
}

