﻿Shader "Unlit/Node"
{
    Properties
    {
		_Color("Main Color", Color) = (1,1,1,1)
		_Thickness("Thickness", Range(0.0,0.5)) = 0.25
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			float4 _Color;
			float _Thickness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed x =  i.uv.x - 0.5;
				fixed y =  i.uv.y - 0.5;
				fixed radius = sqrt(x*x + y*y);
				fixed4 col = float4(1, 1, 1, 1);
				if (radius > 0.5)
				{
					col.a = 0.0f;
					return col;
				}
				if (radius > _Thickness)
				{
					col = _Color;
				}
                return col;
            }
            ENDCG
        }
    }
}
