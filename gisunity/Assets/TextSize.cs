﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSize : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetTextSize();
    }

    public void SetTextSize()
    {
        graph.SetTextSize(slider.value);
    }
}
