﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using UnityEngine.U2D;

public class LoopEdge : MonoBehaviour, IEdge
{
    private Node node;
    public GameObject arrowEnd;
    public GameObject arrow;

    public GameObject textPlace;
    public TextMeshPro label;

    public SpriteShapeController edge;
    private Spline spline;

    private bool isDirectedGraph;
    private float textDistance = 0.7f;

   internal static float loopSize { get; set; }

    void Awake()
    {
        spline = edge.spline;
    }

    void IEdge.Translate(Vector3 positionDelta, bool isDirectedGraph, float nodeRadius)
    {
        var rotation = - positionDelta.x * 5;
        transform.Rotate(node.transform.forward, rotation);
        edge.transform.rotation = Quaternion.identity;
        SetEdgePosition(nodeRadius);
    }

    void IEdge.SetNodes(Tuple<Node, Node> nodes)
    {
        this.node = nodes.Item1;
        node.HasLoop = true;
    }

    void IEdge.Unlink()
    {
        node.RemoveEdge(this);
        node.HasLoop = false;
    }


    void IEdge.AdjustTransform(float arrowLenght,bool isDirectedGraph, float nodeRadius)
    {
        var p2 = SetEdgePosition(nodeRadius);
        SetArrowLenght(arrowLenght, p2);
    }

    Vector3 SetEdgePosition(float nodeRadius)
    {
        var p = node.transform.position + transform.up * nodeRadius;
        transform.position = p;

        edge.transform.position = Vector3.zero;
        var v1 = (-transform.right + -transform.up) * nodeRadius;
        var v2 = (transform.right  + -transform.up) * nodeRadius;
        var p1 = p + v1 * 0.2f;
        var p2 = p + v2 * 0.2f;

        spline.SetPosition(0, p1);
        spline.SetPosition(1, p2);

        var t1 = v2 * loopSize;
        var t2 = v1 * loopSize;

        if (isDirectedGraph)
        {
            spline.SetPosition(1, arrowEnd.transform.position);
            t2 = arrowEnd.transform.forward * t2.magnitude;
        }

        spline.SetLeftTangent(0, t1);
        spline.SetRightTangent(0, -t1);
        spline.SetLeftTangent(1, -t2);
        spline.SetRightTangent(1, t2);

        edge.BakeCollider();
        return MoveToNodeRadius(p2,nodeRadius);
    }

    Vector3 MoveToNodeRadius(Vector3 p, float nodeRadius)
    {
        var distance = Vector3.Distance(p, node.transform.position);
        var distanceToMove = nodeRadius - distance;
        return Vector3.MoveTowards(p,node.transform.position,-distanceToMove);
    }

    private void SetArrowLenght(float arrowLenght, Vector3 p2)
    {
        var scale = arrow.transform.localScale;
        scale.z = arrowLenght;
        arrow.transform.localScale = scale;
        arrow.transform.position = p2;
        arrow.transform.LookAt(node.transform.position,Vector3.forward);
    }


    void IEdge.SetDirectedGraph(bool isOn)
    {
        arrow.SetActive(isOn);
        this.isDirectedGraph = isOn;
    }


    void IEdge.SetLineThickness(float thickness)
    {

    }

    void IEdge.SetArrowThickness(float thickness)
    {
        var arrowScale = arrow.transform.localScale;
        arrowScale.x = thickness;
        arrowScale.y = thickness;
        arrow.transform.localScale = arrowScale;
    }


    void IEdge.SetRotation(Quaternion rotation)
    {
        label.transform.rotation = rotation;
        SetTextPosition();
    }

    private void SetTextPosition()
    {
        label.transform.position = textPlace.transform.position  + textPlace.transform.right * textDistance;
    }

    void IEdge.SetTextSize(float value)
    {
        label.transform.localScale = new Vector3(value,value,value);
    }
}
