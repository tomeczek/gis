﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeLine : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetEdgeThickness();
    }

    public void SetEdgeThickness()
    {
        graph.SetEdgeThickness( slider.value);
    }
}
