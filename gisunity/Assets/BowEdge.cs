﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.U2D;


public class BowEdge : MonoBehaviour, IEdge
{
    public static float bowStraightEdgeRatio = 2f;
    public static float distanceFromStraightEdge = 0.3f;
    public static float tangentModifier = 0.5f;

    private Node node1;
    private Node node2;
    public TextMeshPro label;
    public UnityEngine.U2D.SpriteShapeController edge;
    public GameObject arrow;

    private Spline spline;
    static private readonly float splinePointsEps = 0.1f;

    void Awake()
    {
        spline = edge.spline;
    }

    void IEdge.Translate(Vector3 positionDelta, bool isDirectedGraph, float nodeRadius)
    {
        label.transform.position += positionDelta;
    }

    void IEdge.SetNodes(Tuple<Node, Node> nodes)
    {
        this.node1 = nodes.Item1;
        this.node2 = nodes.Item2;
    }

    public void SetLabel(Tuple<Node, Node> nodes)
    {
        label.transform.position = Vector3.Lerp(nodes.Item1.transform.position, nodes.Item2.transform.position, 0.34f);
    }


    void IEdge.Unlink()
    {
        node1.RemoveEdge(this);
        node2.RemoveEdge(this);
    }

    void IEdge.AdjustTransform(float arrowLenght,bool isDirectedGraph, float nodeRadius)
    {
        transform.position = Vector3.Lerp(node1.transform.position, node2.transform.position, 0.5f);

        edge.transform.position = Vector3.zero;
        var distance = node2.transform.position - node1.transform.position;
        var offset = Vector3.Normalize(Vector3.Cross(distance, Vector3.forward)) * distanceFromStraightEdge;

        var midPoints = GetMidPoints(node1.transform.position, node2.transform.position, nodeRadius, offset);

        SetArrow(arrowLenght, Vector3.MoveTowards(node2.transform.position, midPoints.Item2, nodeRadius));

        var p1 = Vector3.MoveTowards(node1.transform.position, midPoints.Item1, nodeRadius * 0.9f); 
        var p2 = Vector3.MoveTowards(node2.transform.position,midPoints.Item2, nodeRadius * 0.9f);

        if (isDirectedGraph)
        {
            p2 = Vector3.MoveTowards(p2, midPoints.Item2, Math.Min(arrowLenght * 0.9f,(p2  - midPoints.Item2).magnitude - splinePointsEps));
        }

        spline.SetPosition(0, p1);
        spline.SetPosition(1, midPoints.Item1);
        spline.SetPosition(2, midPoints.Item2);
        spline.SetPosition(3, p2);

        var tangent = Vector3.Normalize(Vector3.Cross(offset, Vector3.forward)) *  tangentModifier;
        if(tangent.magnitude > Vector3.Distance(midPoints.Item1, midPoints.Item2))
        {
            tangent = tangent * (Vector3.Distance(midPoints.Item1, midPoints.Item2)/ tangent.magnitude) ;
        }


        spline.SetLeftTangent(1, tangent);
        spline.SetRightTangent(1, -tangent);

        spline.SetLeftTangent(2, tangent);
        spline.SetRightTangent(2, -tangent);
        if (isDirectedGraph)
        {
            spline.SetRightTangent(2, -tangent * 0.1742857f);
        }
        edge.BakeCollider();

    }

    private Tuple<Vector3, Vector3> GetMidPoints(Vector3 position1, Vector3 position2, float nodeRadius, Vector3 offset)
    {
        var proportionalP1 = Vector3.Lerp(position1, position2, 0.4f) + offset;
        var constP1 = Vector3.MoveTowards(position1, position2, nodeRadius * bowStraightEdgeRatio) + offset;
        if( Vector3.Distance(position1, proportionalP1) < Vector3.Distance(position1, constP1))
        {
            var proportionalP2 = Vector3.Lerp(position1, position2, 0.6f) + offset;
            return new Tuple<Vector3, Vector3>(proportionalP1, proportionalP2);
        }
        var constP2 = Vector3.MoveTowards(position2, position1, nodeRadius * bowStraightEdgeRatio) + offset;
        return new Tuple<Vector3, Vector3>(constP1, constP2);
    }

    private void SetArrow(float arrowLenght, Vector3 p2)
    {
        var scale = arrow.transform.localScale;
        scale.z = arrowLenght ;
        arrow.transform.localScale = scale;
        arrow.transform.position = p2;
        arrow.transform.LookAt(node2.transform.position);

    }

    void IEdge.SetDirectedGraph(bool isOn)
    {
        arrow.SetActive(isOn);
    }


    void IEdge.SetLineThickness(float thickness)
    {

    }

    void IEdge.SetArrowThickness(float thickness)
    {
        var scale = arrow.transform.localScale;
        scale.x = thickness;
        scale.y = thickness;
        arrow.transform.localScale = scale;
    }

    void IEdge.SetRotation(Quaternion rotation)
    {
        label.transform.rotation = rotation;
    }
    void IEdge.SetTextSize(float value)
    {
        label.transform.localScale = new Vector3(value, value, value);
    }
}
