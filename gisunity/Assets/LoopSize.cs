﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopSize : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetLoopSize();
    }

    public void SetLoopSize()
    {
        graph.SetLoopSize(slider.value);
    }
}
