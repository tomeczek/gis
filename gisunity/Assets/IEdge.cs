﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IEdge
{

    void SetNodes(Tuple<Node, Node> nodes);

    void Unlink();
    
    void AdjustTransform(float arrowLenght, bool isDirectedGraph, float nodeRadius);

    void SetDirectedGraph(bool isOn);
    
    void SetLineThickness(float thickness);

    void SetArrowThickness(float thickness);

    void SetRotation(Quaternion rotation);

    void Translate(Vector3 positionDelta, bool isDirectedGraph, float nodeRadius);

    void SetTextSize(float value);
}
