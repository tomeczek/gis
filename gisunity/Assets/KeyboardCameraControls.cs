﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class KeyboardCameraControls : MonoBehaviour
{
    public Graph graph;

    public float initialSpeed = 10f;
    public float increaseSpeed = 1.25f;

    public bool allowMovement = true;
    public bool allowRotation = true;

    public KeyCode forwardButton = KeyCode.W;
    public KeyCode backwardButton = KeyCode.S;
    public KeyCode rightButton = KeyCode.D;
    public KeyCode leftButton = KeyCode.A;
    public KeyCode upButton = KeyCode.Q;
    public KeyCode downButton = KeyCode.E;

    public float cursorSensitivity = 0.025f;

    private float currentSpeed = 0f;
    private bool lastMoving = false;


    private void Start()
    {
        graph.SetGraphRotation(transform.rotation);
    }

    private void Update()
    {
        Movement();
        Rotation();
    }

    void Movement()
    {
        if (allowMovement)
        {
            UpdateCurrentSpeed();
            Vector3 deltaPosition = GetDeltaPosition();
            Move(deltaPosition);
        }
    }
    private void UpdateCurrentSpeed()
    {
        if (lastMoving)
            currentSpeed += increaseSpeed * Time.deltaTime;
        else
            currentSpeed = initialSpeed;
    }

    Vector3 GetDeltaPosition()
    {
        Vector3 deltaPosition = Vector3.zero;

        CheckMove(forwardButton, ref deltaPosition, transform.forward);
        CheckMove(backwardButton, ref deltaPosition, -transform.forward);
        CheckMove(rightButton, ref deltaPosition, transform.right);
        CheckMove(leftButton, ref deltaPosition, -transform.right);
        CheckMove(upButton, ref deltaPosition, transform.up);
        CheckMove(downButton, ref deltaPosition, -transform.up);

        deltaPosition = Vector3.Normalize(deltaPosition);
        return deltaPosition;
    }

    private void CheckMove(KeyCode keyCode, ref Vector3 deltaPosition, Vector3 directionVector)
    {
        if (Input.GetKey(keyCode))
        {
            deltaPosition += directionVector;
        }
    }

    private void Move(Vector3 deltaPosition)
    {
        bool moving = deltaPosition != Vector3.zero;
        lastMoving = moving;
        if (moving)
        {
            transform.position += deltaPosition * currentSpeed * Time.deltaTime;
        }
    }


    void Rotation()
    {
        if (allowRotation)
        {
            Vector3 eulerAngles = GetRotation(transform.eulerAngles);
            transform.eulerAngles = eulerAngles;
            graph.SetGraphRotation(transform.rotation);

        }
    }
    private Vector3 GetRotation(Vector3 eulerAngles)
    {
        eulerAngles.x += -Input.GetAxis("Mouse Y") * 359f * cursorSensitivity;
        eulerAngles.y += Input.GetAxis("Mouse X") * 359f * cursorSensitivity;
        return eulerAngles;
    }
}
