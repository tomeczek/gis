﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StraightEdge : MonoBehaviour, IEdge
{
    private Node node1;
    private Node node2;
    public GameObject cube;
    public GameObject arrow;
    public TextMeshPro label;


    void IEdge.Translate(Vector3 positionDelta, bool isDirectedGraph, float nodeRadius)
    {
        label.transform.position += positionDelta;
    }

    void IEdge.SetNodes(Tuple<Node, Node> nodes)
    {
        this.node1 = nodes.Item1;
        this.node2 = nodes.Item2;
    }

    void IEdge.Unlink()
    {
       node1.RemoveEdge(this);
       node2.RemoveEdge(this);
    }


    void IEdge.AdjustTransform(float arrowLenght,bool isDirectedGraph, float nodeRadius)
    {
        var normal = - node1.transform.forward;
        var mid2mid = node1.transform.position - node2.transform.position;
        var translation = Vector3.ProjectOnPlane(mid2mid, normal);
        translation.Normalize();
        translation *= nodeRadius;//scale;
        var p1 = node1.transform.position - translation;
        var p2 = node2.transform.position + translation;
        transform.LookAt(p2, Vector3.forward);
        ScaleCubeToLinkNodes(p1,p2, arrowLenght, isDirectedGraph);
        ScaleArrowToLinkNodes(arrowLenght, p2);
    }

    void IEdge.SetDirectedGraph(bool isOn)
    {
        arrow.SetActive(isOn);
    }

    private void ScaleArrowToLinkNodes(float arrowLenght, Vector3 p2)
    {
        var scale = arrow.transform.localScale;
        scale.z = arrowLenght;
        arrow.transform.localScale = scale;
        arrow.transform.position = p2;
    }

    private void ScaleCubeToLinkNodes(Vector3 p1, Vector3 p2, float arrowLenght, bool isDirectedGraph)
    {
        if (isDirectedGraph)
        {
            p2 = Vector3.MoveTowards(p2, p1, arrowLenght);
        }
        transform.position = Vector3.Lerp(p1, p2, 0.5f);
        var distance = Vector3.Distance(p1, p2);
        var scale = cube.transform.localScale;
        scale.z = distance;
        cube.transform.localScale = scale;
    }

    void IEdge.SetLineThickness(float thickness)
    {
        var scale = cube.transform.localScale;
        scale.x = thickness;
        scale.y = thickness;
        cube.transform.localScale = scale;
    }

     void IEdge.SetArrowThickness(float thickness)
    {
        var scale = arrow.transform.localScale;
        scale.x = thickness;
        scale.y = thickness;
        arrow.transform.localScale = scale;
    }

    void IEdge.SetRotation(Quaternion rotation)
    {
        label.transform.rotation = rotation;
    }

    void IEdge.SetTextSize(float value)
    {
        label.transform.localScale = new Vector3(value, value, value);
    }
}
