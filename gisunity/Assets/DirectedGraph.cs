﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectedGraph : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Toggle toggle;

    private void Start()
    {
        SetDirectedGraph();
    }

    public void SetDirectedGraph()
    {
        graph.SetDirectedGraph(toggle.isOn);
    }
}
