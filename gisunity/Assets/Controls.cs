﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

[RequireComponent(typeof(Camera))]
public class Controls : MonoBehaviour
{
    private Vector3 oldMousePosition = new Vector3();
    private RaycastHit hit;
    private Ray ray;
    private const int onlyFloorLayer = 1 << 9;
    private const int GraphLayer = 1 << 10;
    private const int UILayer = 1 <<5;

    private readonly int MAX_NAME_LENGTH = 2;
    private Dictionary<KeyCode,Vector3> keyDownPositions = new Dictionary<KeyCode, Vector3>();

    public CameraTransform cameraTransform;
    public Graph graph;
    public GameObject HUD;
    public TMP_InputField fileNameInput;
    public UnityEngine.UI.Toggle edgeDrawToggle;
    public bool allowRotation = false;
    private GameObject lastClickedGraphElement = null;
    private GameObject dragGraphElement = null;
    private bool IsPrintMode = false;

    void Start()
    {
        oldMousePosition = GetReyHitPoint();
        graph.SetGraphRotation(cameraTransform.transform.rotation);
    }

    void Update()
    {
        SwitchToPrintMode(KeyCode.Tab);
        CreateNode(KeyCode.Mouse0);
        DeleteGraphElement(KeyCode.Mouse1, KeyCode.Delete);
        ChangeName();
        Movement(KeyCode.Mouse0);
        if(allowRotation)
            Rotation(KeyCode.Mouse1);
        StartCoroutine(TakeScreenShoot(KeyCode.Return));
    }

    private IEnumerator TakeScreenShoot(KeyCode keypadEnter)
    {
        if (Input.GetKeyDown(keypadEnter))
        {
            var fileName = String.Format("{0}.png", fileNameInput.text);

            HUD.SetActive(false);
            if (lastClickedGraphElement)
            {
               Paint(lastClickedGraphElement, Color.black);
            }

            ScreenCapture.CaptureScreenshot(fileName,2);
            yield return 0;
            HUD.SetActive(!IsPrintMode);
            if (lastClickedGraphElement)
            {
                if (IsPrintMode)
                    Paint(lastClickedGraphElement, Color.black);
                else
                    Paint(lastClickedGraphElement, Color.red);
            }
        }
     }

    private void SwitchToPrintMode(KeyCode tab)
    {
        if (Input.GetKeyDown(tab))
        {
            IsPrintMode = !IsPrintMode;
            HUD.SetActive(!IsPrintMode);
            if (lastClickedGraphElement)
            {
                if (IsPrintMode)
                    Paint(lastClickedGraphElement, Color.black);
                else
                    Paint(lastClickedGraphElement, Color.red);
            }
        }
    }

    private void CreateNode(KeyCode keyCode)
    {
        if (IsClick(keyCode) && !EventSystem.current.IsPointerOverGameObject())
        {
            var clickedGraphElement = GetClickedElement(GraphLayer);

            if (lastClickedGraphElement
            && lastClickedGraphElement.GetComponent<Node>()
            && clickedGraphElement
            && clickedGraphElement.GetComponent<Node>()
            && edgeDrawToggle.isOn)
            {
                graph.CreateEdge(lastClickedGraphElement, clickedGraphElement);
            }

            if (!clickedGraphElement)
            {
                clickedGraphElement = graph.CreateNode(GetReyHitPoint());
            }

            SetFocusedElement(clickedGraphElement);

        }
    }

    private void SetFocusedElement(GameObject clickedGraphElement)
    {
        if (lastClickedGraphElement)
            Paint(lastClickedGraphElement, Color.black);

        lastClickedGraphElement = clickedGraphElement;
        if (!IsPrintMode)
        {
            Paint(clickedGraphElement, Color.red);
        }
    }

    private void Paint(GameObject graphElement, Color color)
    {
        var cs = graphElement.GetComponentsInChildren<Renderer>();
        foreach (var c in cs)
        {
            c.material.color = color;
        }
    }

    private void DeleteGraphElement(KeyCode mouseKeyCode, KeyCode keyboardKeyCode)
    {
        if (IsClick(mouseKeyCode) || Input.GetKeyUp(keyboardKeyCode))
        {
            var clickedGraphElement = GetClickedElement(GraphLayer);
            if (clickedGraphElement)
            {
                graph.DeleteGraphElement(clickedGraphElement);
            }
        }
    }

    private GameObject GetClickedElement(int layerMask)
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, Mathf.Infinity,GraphLayer);
        var g = hit.transform?.parent.gameObject;
        if (g == null)
        {
            // https://answers.unity.com/questions/596792/raycast-on-a-2d-collider.html
            var h = Physics2D.Raycast(ray.origin, ray.direction,GraphLayer);
            
            g = h.transform?.parent.gameObject;
        }
        return g;
    }
    

    private bool IsClick(KeyCode keyCode)
    {
        if (Input.GetKeyDown(keyCode))
        {
            keyDownPositions[keyCode] = GetReyHitPoint();
        }
        if (Input.GetKeyUp(keyCode))
        {
            var upPosition = GetReyHitPoint();
            var distance = (keyDownPositions[keyCode] - upPosition).magnitude;
            if (distance < 0.00000005f)
            {
                return true;
            }
        }
        return false;
    }

    private void ChangeName()
    {
       if(lastClickedGraphElement && Input.anyKey)
        {
            var text = lastClickedGraphElement.GetComponentInChildren<TextMeshPro>();
            if (Input.inputString.IndexOf('\b') != -1)
            {
                text.text = String.Empty;
                return;
            }
            text.text = NormalizeLength(text.text + Input.inputString.Trim(), MAX_NAME_LENGTH);
        }
    }

    string NormalizeLength(string value, int maxLength)
    {
        return value.Length <= maxLength ? value : value.Substring(0, maxLength);
    }

    private void Movement(KeyCode keyCode)
    {
        var somethingMoved = false;
        GraphElementTranslation(keyCode,ref  somethingMoved);
        CameraMovement(keyCode, ref somethingMoved);
        oldMousePosition = GetReyHitPoint();
    }

    private void GraphElementTranslation(KeyCode keyCode, ref bool moved)
    {
        if (moved)
        {
            return;
        }
        if (Input.GetKeyDown(keyCode))
        {
            dragGraphElement = GetClickedElement(GraphLayer);
        }
        if (Input.GetKey(keyCode) && dragGraphElement)
        {
            graph.TranslateGraphElement(dragGraphElement, -GetPositionDelta());
            moved = true;
        }
        if (Input.GetKeyUp(keyCode))
        {
            if (dragGraphElement)
                SetFocusedElement(dragGraphElement);
            dragGraphElement = null;
        }
    }

    private void CameraMovement(KeyCode keyCode, ref bool moved)
    {
        if (moved)
        {
            return;
        }
        if (Input.GetKey(keyCode) && !dragGraphElement)
        {
            cameraTransform.UpdatePosition(GetPositionDelta());
            moved = true;
        }
    }

    private void Rotation(KeyCode keyCode)
    {
        if (Input.GetKey(keyCode))
        {
            cameraTransform.UpdateRotation(GetRotationDelta());
            graph.SetGraphRotation(cameraTransform.transform.rotation);
        }
    }

    private Vector3 GetPositionDelta()
    {
        return oldMousePosition - GetReyHitPoint(); 
    }

    private Vector3 GetReyHitPoint()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, Mathf.Infinity, onlyFloorLayer);
        return hit.point;
    }

    private Vector3 GetRotationDelta()
    {
        Vector3 rotation = Vector3.zero;
        rotation.x = -Input.GetAxis("Mouse Y") * 359f * 0.025f;
        rotation.y = Input.GetAxis("Mouse X") * 359f * 0.025f;
        return rotation;
    }
}