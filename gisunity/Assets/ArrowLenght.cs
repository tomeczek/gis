﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLenght : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetArrowLenght();
    }

    public void SetArrowLenght()
    {
        graph.SetArrowLenght( slider.value);
    }
}
