﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeSize : MonoBehaviour
{
    public Graph graph;
    public UnityEngine.UI.Slider slider;

    private void Start()
    {
        SetNodeSize();
    }

    public void SetNodeSize()
    {
        graph.SetNodeDiameter( slider.value);
    }
}
